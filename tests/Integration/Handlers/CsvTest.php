<?php

namespace Tests\Smorken\Export\Integration\Handlers;

use Illuminate\Support\Collection;
use League\Csv\Writer;
use Mockery as m;
use PHPUnit\Framework\TestCase;
use Tests\Smorken\Export\Stubs\GetAttrs;
use Tests\Smorken\Export\Stubs\ToArrayM;
use Tests\Smorken\Export\Stubs\VONoMethods;
use Tests\Smorken\Export\Stubs\VOWithGetAttrs;
use Tests\Smorken\Export\Stubs\VOWithToArray;

class CsvTest extends TestCase
{
    public function tearDown(): void
    {
        m::close();
    }

    public function testRunCollectionWithCallbacksOnHeader()
    {
        $header_func = function ($me, $headers, $first) {
            $headers[] = 'new_header';

            return $headers;
        };
        $sut = $this->getSut();
        $models = new Collection([
            new GetAttrs(['foo' => 'fooval1', 'bar' => 'barval1', 'fiz' => 'fizval1']),
            new GetAttrs(['foo' => 'fooval2', 'bar' => 'barval2', 'fiz' => 'fizval2']),
            new GetAttrs(['foo' => 'fooval3', 'bar' => 'barval3', 'fiz' => 'fizval3']),
        ]);
        $r = $this->runExport($sut, $models, $header_func);
        //$r = $sut->run('export', $models, $header_func);
        $this->compareResults("foo,bar,fiz,new_header\nfooval1,barval1,fizval1\nfooval2,barval2,fizval2\nfooval3,barval3,fizval3\n",
            $r);
    }

    public function testRunCollectionWithCallbacksOnRow()
    {
        $row_func = function ($me, $data, $models) {
            $data['bar'] = 'updated';

            return [$data];
        };
        $sut = $this->getSut();
        $models = new Collection([
            new GetAttrs(['foo' => 'fooval1', 'bar' => 'barval1', 'fiz' => 'fizval1']),
            new GetAttrs(['foo' => 'fooval2', 'bar' => 'barval2', 'fiz' => 'fizval2']),
            new GetAttrs(['foo' => 'fooval3', 'bar' => 'barval3', 'fiz' => 'fizval3']),
        ]);
        $r = $this->runExport($sut, $models, null, $row_func);
        //$r = $sut->run('export', $models, null, $row_func);
        $this->compareResults("foo,bar,fiz\nfooval1,updated,fizval1\nfooval2,updated,fizval2\nfooval3,updated,fizval3\n",
            $r);
    }

    public function testRunCollectionWithCallbacksRelationType()
    {
        $row_func = function ($me, $data, $models) {
            $fiz = $data['fiz'];
            $data['fiz'] = 'id';
            $fizdata = $me->getAsArray($fiz);
            foreach ($fizdata as $k => $v) {
                $data[$k] = $v;
            }

            return [$data];
        };
        $header_func = function ($me, $headers, $first) {
            $headers = array_merge($headers, $me->getHeadersFromModel($first->fiz));

            return $headers;
        };
        $sut = $this->getSut();
        $models = new Collection([
            new GetAttrs(['foo' => 'fooval1', 'bar' => 'barval1', 'fiz' => new VONoMethods(['biz' => 'baz1'])]),
            new GetAttrs(['foo' => 'fooval2', 'bar' => 'barval2', 'fiz' => new VONoMethods(['biz' => 'baz2'])]),
            new GetAttrs(['foo' => 'fooval3', 'bar' => 'barval3', 'fiz' => new VONoMethods(['biz' => 'baz3'])]),
        ]);
        $r = $this->runExport($sut, $models, $header_func, $row_func);
        //$r = $sut->run('export', $models, $header_func, $row_func);
        $this->compareResults("foo,bar,fiz,biz\nfooval1,barval1,id,baz1\nfooval2,barval2,id,baz2\nfooval3,barval3,id,baz3\n",
            $r);
    }

    public function testRunCollectionWithoutCallbacksOnModelWithToArray()
    {
        $sut = $this->getSut();
        $models = new \Illuminate\Support\Collection([
            new ToArrayM(['foo' => 'fooval1', 'bar' => 'barval1', 'fiz' => 'fizval1']),
            new ToArrayM(['foo' => 'fooval2', 'bar' => 'barval2', 'fiz' => 'fizval2']),
            new ToArrayM(['foo' => 'fooval3', 'bar' => 'barval3', 'fiz' => 'fizval3']),
        ]);
        $r = $this->runExport($sut, $models);
        //$r = $sut->run('export', $models);
        $this->compareResults("foo,bar,fiz\nfooval1,barval1,fizval1\nfooval2,barval2,fizval2\nfooval3,barval3,fizval3\n",
            $r);
    }

    public function testFileWithoutCallbacksOnArray()
    {
        $sut = $this->getSut();
        $models = [
            ['foo' => 'fooval1', 'bar' => 'barval1', 'fiz' => 'fizval1'],
            ['foo' => 'fooval2', 'bar' => 'barval2', 'fiz' => 'fizval2'],
            ['foo' => 'fooval3', 'bar' => 'barval3', 'fiz' => 'fizval3'],
        ];
        $sut->header($models);
        $sut->process($models);
        $r = $sut->file('test', '/app/tests');
        $this->assertTrue($r);
    }

    public function testRunWithoutCallbacksOnArray()
    {
        $sut = $this->getSut();
        $models = [
            ['foo' => 'fooval1', 'bar' => 'barval1', 'fiz' => 'fizval1'],
            ['foo' => 'fooval2', 'bar' => 'barval2', 'fiz' => 'fizval2'],
            ['foo' => 'fooval3', 'bar' => 'barval3', 'fiz' => 'fizval3'],
        ];
        $r = $this->runExport($sut, $models);
        //$r = $sut->run('export', $models);
        $this->compareResults("foo,bar,fiz\nfooval1,barval1,fizval1\nfooval2,barval2,fizval2\nfooval3,barval3,fizval3\n",
            $r);
    }

    public function testRunWithoutCallbacksOnModelWithGetAttributes()
    {
        $sut = $this->getSut();
        $models = [
            new GetAttrs(['foo' => 'fooval1', 'bar' => 'barval1', 'fiz' => 'fizval1']),
            new GetAttrs(['foo' => 'fooval2', 'bar' => 'barval2', 'fiz' => 'fizval2']),
            new GetAttrs(['foo' => 'fooval3', 'bar' => 'barval3', 'fiz' => 'fizval3']),
        ];
        $r = $this->runExport($sut, $models);
        //$r = $sut->run('export', $models);
        $this->compareResults("foo,bar,fiz\nfooval1,barval1,fizval1\nfooval2,barval2,fizval2\nfooval3,barval3,fizval3\n",
            $r);
    }

    public function testRunWithoutCallbacksOnModelWithToArray()
    {
        $sut = $this->getSut();
        $models = [
            new ToArrayM(['foo' => 'fooval1', 'bar' => 'barval1', 'fiz' => 'fizval1']),
            new ToArrayM(['foo' => 'fooval2', 'bar' => 'barval2', 'fiz' => 'fizval2']),
            new ToArrayM(['foo' => 'fooval3', 'bar' => 'barval3', 'fiz' => 'fizval3']),
        ];
        $r = $this->runExport($sut, $models);
        //$r = $sut->run('export', $models);
        $this->compareResults("foo,bar,fiz\nfooval1,barval1,fizval1\nfooval2,barval2,fizval2\nfooval3,barval3,fizval3\n",
            $r);
    }

    public function testRunWithoutCallbacksOnVo()
    {
        $sut = $this->getSut();
        $models = [
            new VONoMethods(['foo' => 'fooval1', 'bar' => 'barval1', 'fiz' => 'fizval1']),
            new VONoMethods(['foo' => 'fooval2', 'bar' => 'barval2', 'fiz' => 'fizval2']),
            new VONoMethods(['foo' => 'fooval3', 'bar' => 'barval3', 'fiz' => 'fizval3']),
        ];
        $r = $this->runExport($sut, $models);
        //$r = $sut->run('export', $models);
        $this->compareResults("foo,bar,fiz\nfooval1,barval1,fizval1\nfooval2,barval2,fizval2\nfooval3,barval3,fizval3\n",
            $r);
    }

    public function testRunWithoutCallbacksOnVoWithGetAttributes()
    {
        $sut = $this->getSut();
        $models = [
            new VOWithGetAttrs(['foo' => 'fooval1', 'bar' => 'barval1', 'fiz' => 'fizval1']),
            new VOWithGetAttrs(['foo' => 'fooval2', 'bar' => 'barval2', 'fiz' => 'fizval2']),
            new VOWithGetAttrs(['foo' => 'fooval3', 'bar' => 'barval3', 'fiz' => 'fizval3']),
        ];
        $r = $this->runExport($sut, $models);
        //$r = $sut->run('export', $models);
        $this->compareResults("barbar\nfooval1\nfooval2\nfooval3\n",
            $r);
    }

    public function testRunWithoutCallbacksOnVoWithToArray()
    {
        $sut = $this->getSut();
        $models = [
            new VOWithToArray(['foo' => 'fooval1', 'bar' => 'barval1', 'fiz' => 'fizval1']),
            new VOWithToArray(['foo' => 'fooval2', 'bar' => 'barval2', 'fiz' => 'fizval2']),
            new VOWithToArray(['foo' => 'fooval3', 'bar' => 'barval3', 'fiz' => 'fizval3']),
        ];
        $r = $this->runExport($sut, $models);
        //$r = $sut->run('export', $models);
        $this->compareResults("barbar\nfooval1\nfooval2\nfooval3\n",
            $r);
    }

    protected function compareResults($expected_string, $result_string)
    {
        $results = explode(PHP_EOL, $result_string);
        $expected = explode(PHP_EOL, $expected_string);
        foreach ($expected as $i => $line) {
            $this->assertEquals($line, $results[$i]);
        }
    }

    protected function getSut()
    {
        $backend = Writer::createFromFileObject(new \SplTempFileObject);
        $sut = new \Smorken\Export\Handlers\Csv($backend);

        return $sut;
    }

    protected function runExport($sut, ...$params)
    {
        ob_start();
        call_user_func_array([$sut, 'run'], array_merge(['export'], $params));

        return ob_get_clean();
    }
}
