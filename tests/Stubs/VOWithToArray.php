<?php

namespace Tests\Smorken\Export\Stubs;

class VOWithToArray extends BaseModel
{
    public function toArray(): array
    {
        return ['barbar' => $this->attrs['foofoo'] ?? null];
    }

    public function toVo(): static
    {
        return new static(['foofoo' => $this->attrs['foo']]);
    }
}
