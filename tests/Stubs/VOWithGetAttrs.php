<?php

namespace Tests\Smorken\Export\Stubs;

class VOWithGetAttrs extends BaseModel
{
    public function getAttributes(): array
    {
        return ['barbar' => $this->attrs['foofoo'] ?? null];
    }

    public function toVo(): static
    {
        return new static(['foofoo' => $this->attrs['foo']]);
    }
}
