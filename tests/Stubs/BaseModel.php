<?php

namespace Tests\Smorken\Export\Stubs;

class BaseModel
{
    protected array $attrs = [];

    public function __construct(array $attrs = [])
    {
        $this->attrs = $attrs;
    }

    public function __get($key)
    {
        return $this->attrs[$key] ?? null;
    }
}
