<?php

namespace Tests\Smorken\Export\Stubs;

class ToArrayM extends BaseModel
{
    public function toArray(): array
    {
        return $this->attrs;
    }
}
