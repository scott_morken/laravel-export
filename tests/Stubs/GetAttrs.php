<?php

namespace Tests\Smorken\Export\Stubs;

class GetAttrs extends BaseModel
{
    public function getAttributes(): array
    {
        return $this->attrs;
    }
}
