<?php

namespace Tests\Smorken\Export\Stubs;

#[\AllowDynamicProperties]
class VONoMethods
{
    public function __construct(array $attrs = [])
    {
        foreach ($attrs as $k => $v) {
            $this->$k = $v;
        }
    }

    public function toVo(): static
    {
        return $this;
    }
}
