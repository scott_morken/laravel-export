<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/15/16
 * Time: 8:57 AM
 */

namespace Smorken\Export;

use League\Csv\Writer;
use Smorken\Export\Contracts\Export;
use Smorken\Export\Handlers\Csv;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    /**
     * Register the service provider.
     */
    public function register(): void
    {
        $this->app->bind(\Smorken\Export\Contracts\Types\Csv::class, function ($app) {
            $backend = Writer::createFromFileObject(new \SplTempFileObject);

            return new Csv($backend);
        });
        $this->bindDefault();
    }

    protected function bindDefault(): void
    {
        $this->app->bind(Export::class, \Smorken\Export\Contracts\Types\Csv::class);
    }
}
