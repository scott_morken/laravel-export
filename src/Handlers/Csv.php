<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 9/1/16
 * Time: 8:36 AM
 */

namespace Smorken\Export\Handlers;

use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Response;
use Smorken\Export\ExportException;

class Csv extends Base
{
    public function file(string $name, string $path): bool
    {
        $realpath = $this->ensureFileStoreDir($path);
        if ($realpath) {
            $filename = implode(DIRECTORY_SEPARATOR, [$realpath, $this->getName($name)]);

            return file_put_contents($filename, (string) $this->backend);
        }

        return false;
    }

    public function header(mixed $model): void
    {
        $headers = $this->getHeaders($model, $this->getCallback('header'));
        $this->backend->insertOne($headers);
    }

    /**
     * @return \Illuminate\Http\Response|void
     */
    public function output(string $name, bool $standard = false)
    {
        if (! $standard && class_exists(Response::class)) {
            try {
                return Response::make((string) $this->backend, 200, [
                    'Content-Type' => 'text/csv',
                    'Content-Disposition' => sprintf('attachment; filename="%s"', $this->getName($name)),
                ]);
            } catch (\RuntimeException) {
                // pass
            }
        }
        echo $this->standardReturn($name);
    }

    public function process(Collection|Paginator|array $models): void
    {
        $models = $this->ensureModels($models);
        foreach ($models as $model) {
            foreach ($this->getRowData($model, $this->getCallback('row')) as $data) {
                if ($data) {
                    $this->backend->insertOne($data);
                }
            }
        }
    }

    /**
     * @return \Illuminate\Http\Response|void
     */
    public function run(
        string $name,
        Collection|Paginator|array $models,
        ?callable $header_callback = null,
        ?callable $row_callback = null
    ) {
        $this->init($header_callback, $row_callback);
        $this->header($models);
        $this->process($models);

        return $this->output($name);
    }

    protected function ensureFileStoreDir(string $path): string
    {
        if (! file_exists($path)) {
            mkdir($path);
        }
        $realpath = realpath($path);
        if (! $realpath) {
            throw new ExportException("Unable to create [$path]");
        }

        return $realpath;
    }

    protected function getName(string $name): string
    {
        return sprintf('%s.%s.csv', $name, time());
    }

    protected function standardReturn(string $name): string
    {
        if (php_sapi_name() !== 'cli') {
            header('Content-Type: text/csv; charset=utf-8');
            header(sprintf('Content-Disposition: attachment; filename="%s"', $this->getName($name)));
        }

        return (string) $this->backend;
    }
}
