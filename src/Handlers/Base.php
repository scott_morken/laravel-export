<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 9/1/16
 * Time: 8:35 AM
 */

namespace Smorken\Export\Handlers;

use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Support\Collection;
use Smorken\Export\Contracts\Export;

abstract class Base implements Export
{
    protected mixed $backend;

    protected array $callbacks = [
        'header' => null,
        'row' => null,
    ];

    public function __construct($backend)
    {
        $this->setBackend($backend);
    }

    public function getAsArray(mixed $model): array
    {
        if (is_object($model)) {
            if (method_exists($model, 'toVo')) {
                $model = $model->toVo();
                if (method_exists($model, 'toArray')) {
                    return $model->toArray();
                }
            }
            if (method_exists($model, 'getData')) {
                return $model->getData()->toArray();
            }
            if (method_exists($model, 'getAttributes')) {
                return $model->getAttributes();
            }
            if (method_exists($model, 'toArray')) {
                return $model->toArray();
            }
        }

        return (array) $model;
    }

    public function getBackend(): mixed
    {
        return $this->backend;
    }

    public function setBackend(mixed $backend): void
    {
        $this->backend = $backend;
    }

    public function getHeadersFromModel(mixed $model): array
    {
        return array_keys($this->getAsArray($model));
    }

    public function getRowData(mixed $model, ?callable $callback = null): array
    {
        $data = $this->getAsArray($model);
        if ($callback) {
            return $callback($this, $data, $model);
        }

        return [$data];
    }

    public function init(?callable $header_callback = null, ?callable $row_callback = null): void
    {
        $this->disableDebugBar();
        $this->callbacks['header'] = $header_callback;
        $this->callbacks['row'] = $row_callback;
    }

    protected function disableDebugBar(): void
    {
        if (function_exists('app') && app()->bound('debugbar')) {
            $debugbar = app('debugbar');
            $debugbar?->disable();
        }
    }

    protected function ensureModels(Collection|Paginator|array $models): Collection|Paginator|array
    {
        if (is_object($models) && method_exists($models, 'items')) {
            $models = $models->items();
        }

        return $models;
    }

    protected function getCallback(string $type): ?callable
    {
        return $this->callbacks[$type] ?? null;
    }

    protected function getFirstModel(Collection|Paginator|array $models): mixed
    {
        $models = $this->ensureModels($models);
        $first = $models;
        if (is_object($models) && method_exists($models, 'first')) {
            $first = $models->first();
        } elseif (is_array($models)) {
            $first = reset($models);
        }

        return $first;
    }

    protected function getHeaders(mixed $model, ?callable $callback = null): array
    {
        $model = $this->getFirstModel($model);
        $headers = $this->getHeadersFromModel($model);
        if ($callback) {
            $headers = $callback($this, $headers, $model);
        }

        return $headers;
    }
}
