<?php

namespace Smorken\Export\Contracts\Types;

use Smorken\Export\Contracts\Export;

interface Csv extends Export {}
