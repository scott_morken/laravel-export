<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 9/1/16
 * Time: 8:32 AM
 */

namespace Smorken\Export\Contracts;

use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Http\Response;
use Illuminate\Support\Collection;

interface Export
{
    public function file(string $name, string $path): bool;

    public function getAsArray(mixed $model): array;

    public function getBackend(): mixed;

    public function getHeadersFromModel(mixed $model): array;

    public function getRowData(mixed $model, ?callable $callback = null): array;

    public function header(mixed $model): void;

    public function init(?callable $header_callback = null, ?callable $row_callback = null): void;

    /**
     * No return type set since it will be either return a response or header and printed data (old
     * school response)
     *
     * @return Response|void
     */
    public function output(string $name, bool $standard = false);

    public function process(Collection|Paginator|array $models): void;

    /**
     * @return \Illuminate\Http\Response|void
     */
    public function run(
        string $name,
        Collection|Paginator|array $models,
        ?callable $header_callback = null,
        ?callable $row_callback = null
    );

    public function setBackend(mixed $backend): void;
}
